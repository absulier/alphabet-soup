# Enlighten Programming Challenge: Alphabet Soup
## December 2020
## Adam Sulier

###
Language: Python

Package Manager: Pip

Runtime: Average of 0.2 milliseconds 

This program is run by executing the python run.py file.
By default, the program will execute on the input from the input.txt file. 
The input can be changed in one of 3 ways:
1) editing line 6 of the run.py file by changing the name of the input to the desired input file.
2) Commenting out line 6 of the run.py file and uncommenting line 7. This method allows user input upon execution of the file
3) Changing the content of the input.txt file to the desired input.

Note: for testing purposes, lines 2,3, and 124 can be uncommented. This will allow for the runtime of the file to print upon the execution of the code after the results. If option 2 is utilized for changing the input, runtime will include the time taken by the user to choose and enter the file.