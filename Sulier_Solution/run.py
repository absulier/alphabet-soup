#gettime stamps for runtime
#import time
#start_time = time.time()

#Load input: list of strings, each string = 1 row
input_data = open('input.txt', 'r').read().splitlines()
#input_data = open(input('Please enter file name:'), 'r').read().splitlines()

#first line defines size
size = input_data[0].split('x')
size[0],size[1] = int(size[0]),int(size[1])

#intialize an empty list to hold the word grid
word_grid = []
#Use the size variable to loop through the input file over the rows that define the word grid 
for i in input_data[1:size[0]+1]:
    #append each row as a sub list 
    word_grid.append(i.split(" "))
#Indexing format: grid[row#][column#]

#initialize an empty list to hold words to be searched for
words = []
#interate over input data starting after the grid to end of file
for i in input_data[size[0]+1:]:
    words.append(i.strip())

#Define function to grab all instance in grid where word could start, returns list of possible start locations
def locate_starter(word):
    locations = []
    #use the size variable to iterate over the grid, row by row
    for row in range(size[0]):
        #check if the first letter of the word is in the row
        if word[0] in word_grid[row]:
            #if letter located in row, iterate over the row, by column entry 
            for column_entry in range(size[1]):
                #save all locations where letter matches column entry
                if word[0] == word_grid[row][column_entry]:
                    locations.append([row,column_entry])
    return(locations)

#define function that takes a word and starter location, and checks all 8 possibile directions for a match
def check_location(word,start_location):
    
    #Check if there is enough room for word to exist in each cardninar direction from location
    #set all to false as default unless proven to be true
    north,south,east,west,northeast,northwest,southeast,southwest=False,False,False,False,False,False,False,False

    if len(word)<= len(word_grid[:start_location[0]+1]):
        north = True
    if len(word)<= len(word_grid[start_location[0]:]):
        south = True
    if len(word)<= len(word_grid[start_location[0]][start_location[1]:]):
        east = True
    if len(word)<= len(word_grid[start_location[0]][:start_location[1]+1]):
        west = True
    if (north == True) and (east == True):
        northeast = True
    if (north == True) and (west == True):  
        northwest = True
    if (south == True) and (east == True):
        southeast = True
    if (south == True) and (west == True):
        southwest = True

    #Check all directions for match, one character at a time
    #if a non-matching character is found, stop checking that direction
    for i in range(len(word)):
        if north == True:
            if word[i] != word_grid[start_location[0]-i][start_location[1]]:
                north = False
        if south == True:
            if word[i] != word_grid[start_location[0]+i][start_location[1]]:
                south = False
        if east == True:
            if word[i] != word_grid[start_location[0]][start_location[1]+i]:
                east = False
        if west == True:
            if word[i] != word_grid[start_location[0]][start_location[1]-i]:
                west = False
        if northeast == True:
            if word[i] != word_grid[start_location[0]-i][start_location[1]+i]:
                northeast = False
        if northwest == True:
            if word[i] != word_grid[start_location[0]-i][start_location[1]-i]:
                northwest = False
        if southeast == True:
            if word[i] != word_grid[start_location[0]+i][start_location[1]+i]:
                southeast = False
        if southwest == True:
           if word[i] != word_grid[start_location[0]+i][start_location[1]-i]:
               southwest = False

    #If a match is found for a direction, add the length of the word to the starting location,
    #in that direction to return the end position of the located word. Return ['X','X'] if no match found at given location
    if north == True:
        return([start_location[0]-(len(word)-1),start_location[1]])
    elif south == True:
        return([start_location[0]+(len(word)-1),start_location[1]])
    elif east == True:
        return([start_location[0],start_location[1]+(len(word)-1)])    
    elif west == True:
        return([start_location[0],start_location[1]-(len(word)-1)])  
    elif northeast == True:
        return([start_location[0]-(len(word)-1),start_location[1]+(len(word)-1)])  
    elif northwest == True:
        return([start_location[0]-(len(word)-1),start_location[1]-(len(word)-1)])  
    elif southeast == True:
        return([start_location[0]+(len(word)-1),start_location[1]+(len(word)-1)])  
    elif southwest == True:
        return([start_location[0]+(len(word)-1),start_location[1]-(len(word)-1)])
    else:
        return(['X','X'])

#check each word
for word in words:
    #check each starter location for the word
    for start in locate_starter(word):
        end = check_location(word,start)
        #stop looking for word if found at any location
        if end != ['X','X']:
            print(word + ' ' + str(start[0]) + ':' + str(start[1]) + ' ' + str(end[0]) + ':' + str(end[1]))
            break

#print("runtime: ", (time.time() - start_time))